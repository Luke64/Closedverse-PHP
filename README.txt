Closedverse PHP is a clone by Luke64 that's basically what it sounds like.
This was made to be a better alternative to the original CV code which
was written in python, overly complicated, probably has files missing,
and not very beginner, or any user of the code in general, friendly. This
Closedverse has awesome new features like advanced theme options, more
community types, ect

notes-

This uses code from Eric and Seth's Cedar and uses CSS and community
artwork from the original Closedverse by Arian Kordi and PF2M. All credit
goes to them for creating these assets. 


Using this clone to make your own is fine with me as long as you change
enough stuff to make it it's own thing. However, if you want to make a
straight up rehost of this clone, please get my approval before doing so
by contacting me on Discord. (Luke64#3140). If you do so without my 
consent, I hold the right to take it down.


We have nothing to do with Nintendo/Hatena. This is simply a non-profit 
fan project.

